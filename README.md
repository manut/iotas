# Iotas note taking

Iotas aims to provide distraction-free note taking via its mobile-first design.

<img src="https://gitlab.gnome.org/World/iotas/-/raw/main/data/screenshots/mobile.png" width="350px" />

## Featuring

- Optional speedy sync with Nextcloud Notes
- Offline note editing, syncing when back online
- Category editing and filtering
- Favourites
- Spell checking
- Search within the collection or individual notes
- Focus mode and an option to hide the editor header bar
- In preview: export to PDF, ODT and HTML
- A convergent design, seeing Iotas as at home on desktop as mobile
- Search from GNOME Shell
- Note backup and restoration (from CLI, for using without sync)
- The ability to change font size and toggle monospace style

Writing in markdown is supported but optional, providing

- Syntax highlighting with themes
- Formatted render view
- The ability to check off task lists from the rendered markdown

Slightly more technical details, for those into that type of thing

- Nextcloud Notes sync is via the REST API, not WebDAV, which makes it snappy
- There's basic sync conflict detection
- Notes are constantly saved
- Large note collections are partially loaded to quicken startup 
- Notes are stored in SQLite, providing for fast search (FTS) without reinventing the wheel. Plain files can be retrieved by making a backup (CLI).

## Install

<a href="https://flathub.org/apps/details/org.gnome.World.Iotas"><img src="https://flathub.org/assets/badges/flathub-badge-i-en.png" width="190px" /></a>

## Issues & contact

Please report any bugs you come across in [the usual place](https://gitlab.gnome.org/World/iotas/-/issues).

## FAQ / tips

**After updating Iotas I'm told I need to reauthenticate with my server, is this legitimate?**

Yes. Iotas recently changed unique identifier as part of its move into GNOME World. Due to this move it can no longer access its previous Nextcloud authentication token.

Please reauthenticate to continue. No data will be lost or duplicated. Sorry for the inconvenience! 

If you skip doing this it will later show as a message about no authentication token being retrieved.

**Why can't I see my notes after updating? or, How do I update beyond v0.2.6?**

Iotas recently changed unique identifier as part of its move into GNOME World. Migration to the version with the new id should be automatic however in rare circumstances you may get stuck on the old version or find yourself in the new version without your notes. The steps below emulate the automated migration. Note that this only applies to Flatpak; this isn't needed for distro packages.

1. If it's running, quit Iotas
2. Backup your notes: `cp ~/.var/app/org.gnome.gitlab.cheywood.Iotas/data/iotas/notes.db ~/iotas-notes-backup.db`
3. Ensure old Iotas is removed: `flatpak remove org.gnome.gitlab.cheywood.Iotas`
4. Ensure latest Iotas is installed: `flatpak install org.gnome.World.Iotas`
5. Open Iotas. You should see an empty window. If you see any notes here DO NOT continue.
6. Quit Iotas
7. Import your notes: `cp ~/.var/app/org.gnome.gitlab.cheywood.Iotas/data/iotas/notes.db ~/.var/app/org.gnome.World.Iotas/data/iotas/`
8. Import your preferences: `cp ~/.var/app/org.gnome.gitlab.cheywood.Iotas/config/glib-2.0/settings/keyfile ~/.var/app/org.gnome.World.Iotas/config/glib-2.0/settings/`
9. If using sync with Nextcloud Notes: open Iotas and re-establish sync via _Sync with Nextcloud Notes_ in the menu

Feel free to [open an issue](https://gitlab.gnome.org/World/iotas/-/issues) if you need some help through this.

**I don't want to use markdown, can I still use Iotas as a simple unformatted note editor?**

Yes, just disable markdown syntax highlighting and the markdown render view.

**What options should I use to make Iotas run fast on my low power device?**

Disable spelling, markdown syntax highlighting and markdown render view.

**Why does my category disappear when I remove all its notes?**

Categories in Iotas only exist when attached to one or more notes. This mirrors the behaviour in Nextcloud Notes and for the moment is unlikely to change unless the server-side behaviour does.

**How do I change the spelling language?**

In the editor open the spelling menu (right-click on desktop, long press on mobile) and navigate to _Languages_.

Notes: 
- On mobile the menu will not appear if you press on the cursor
- Debian/Ubuntu packages before v0.2.7 were crippled, removing spell checking

**How do I add extra languages for spell checking?**

You need to configure Flatpak for the languages you desire using the two letter language codes (ISO 639-1). 

eg. to add Spanish to an English system

1. Update the Flatpak language configuration, placing a semicolon between the language codes `flatpak config languages --set "en;es"`
2. Pull locale updates for the new setting `sudo flatpak update`
3. Relaunch Iotas

If you have installed Iotas via another means make sure you have the dictionaries installed for the languages you want to use.

**How do I change the font used in the editor?**

Iotas uses GNOME's font settings to determine which font to use. Changing the monospace or document font in GNOME Tweaks will update the editor font used, depending on whether using a monospace font is selected in the preferences.

**Why have the ability to backup and restore notes when sync with Nextcloud Notes is supported?**

The backups are for people who don't have access to a Nextcloud instance or would rather manage their data another way. More broadly this ties into local-first computing.

**How do I use the backups?**

Create a backup with `flatpak run org.gnome.World.Iotas --create-backup`.

To find out where that backup use `flatpak run org.gnome.World.Iotas --display-backup-path`.

Whatever mechanism can then be used to transfer that path (eg. rsync, Syncthing).

A backup can be restored via `flatpak run org.gnome.World.Iotas --restore-backup`.

Notes:
- Backups are restored from the path displayed with `--display-backup-path`
- Backups can only be restored when there are no existing notes
- Backups can't be restored when synced to Nextcloud Notes. In that case your backups should be via the server instead.
- Backups can't be created or restored when Iotas is already running. If you want to automate this you can call `flatpak run org.gnome.World.Iotas --quit-running` beforehand.

**Why isn't the backup placed somewhere more conventional?**

With Flatpak accessing more of the filesystem would require an extra permission, which we don't want to use.

**Can I change the file extension used for backups?**

Yes. For example to change to `.md`:

1. Run a shell within the Flatpak sandbox: `flatpak run --command=sh org.gnome.World.Iotas`
2. Toggle the setting in that shell: `gsettings set org.gnome.World.Iotas backup-note-extension md`

**Why don't my backup files have the same file extension as they do on my Nextcloud instance?**

The Nextcloud Notes API doesn't provide access to the file extension for each file. 

There's a server side preference for file extensions that Iotas will look to follow in the future. However on the server that preference doesn't impact the file extension for notes created before the preference existed.

**Can I disable the editor headerbar being hidden when fullscreen?**

Yes.

1. If not already visible, show the extended preferences: `flatpak run org.gnome.World.Iotas --toggle-extended-preferences`
2. Toggle _Hide Headerbar When Fullscreen_ in the preferences

**Can I use my own markdown syntax theme?**

Yes, place it in `~/.var/app/org.gnome.World.Iotas/data/gtksourceview-5/styles/`. Copy one of the [existing themes](https://gitlab.gnome.org/World/iotas/-/tree/main/data/gtksourceview-5/styles) as a base. You should end up with a `<theme_name>.xml` and a `<theme_name>-dark.xml` in that directory.

**How do I sign out from sync with Nextcloud Notes without losing all my notes?**

1. Perform a backup (from the command line, see above)
2. Sign out from sync
3. Restore the backup

Note that if you then re-establish sync with the same Nextcloud instance you will likely end up with a mess.

**How do I use Iotas with a Nextcloud host using a self-signed SSL certificate?**

1. Log in to your Nextcloud instance in Firefox
2. Click on the padlock next to the URL
3. Drill down through _Connection not secure_ > _More information_ > _View certificate_
4. Download the _PEM (chain)_ in the _Miscellaneous_ section (not the _PEM (cert)_)
7. Place (and rename) the file to the location provided by `flatpak run org.gnome.World.Iotas --display-ca-file-path`
6. Restart Iotas and log in to your server

If you've created a custom CA see below instead.

**How do I use Iotas with a Nextcloud host using a my own certificate authority (CA)?**

1. Follow the steps above for self-signed certificates to deploy the certificate chain from Firefox
2. Ensure you have your CA's certificate as a `.pem` file. You may need to convert it eg. `openssl x509 -inform der -in input.cer -out output.pem`
3. Append the contents of your CA's `.pem` to the certificate chain created in step \#5 above
4. Restart Iotas and log in to your server

**Can support for export to format xyz be added?**

Iotas uses [pandoc](https://pandoc.org/) for some of its exporting and extra formats can be enabled. Note that these will receive less support than the core formats and formats for which pandoc depends on TeX won't work.

1. Find the [pandoc identifier for the format](https://pandoc.org/MANUAL.html#option--to)
2. Build a JSON array for the formats. eg. 

    ```
    [
      {"pandocOutFormat": "docx", "fileExtension": "docx"},
      {"pandocOutFormat": "epub3", "fileExtension": "epub"}
    ]
    ```

2. Run a shell within the Flatpak sandbox: `flatpak run --command=sh org.gnome.World.Iotas`
3. Flatten and escape the JSON to set the formats in that shell: `gsettings set org.gnome.World.Iotas extra-export-formats "[{\"pandocOutFormat\": \"docx\", \"fileExtension\": \"docx\"}, {\"pandocOutFormat\": \"epub3\", \"fileExtension\": \"epub\"}]"`

**Why does Iotas export to a strange directory?**

Saving to that directory prevents Iotas (Flatpak) from needing access to the filesystem, which in most cases is an unnecessary permission. If you would like to save to other locations [Flatseal](https://flathub.org/apps/com.github.tchx84.Flatseal) can be used to give Iotas access to your filesystem. Once the permission has been granted you will be to choose a location during export.

Note: as a minimum add access to your documents folder as it's used as a test to determine whether user directories are accessible.

**How do I enable the experimental TeX math equation support?**

1. If not already visible, show the extended preferences: `flatpak run org.gnome.World.Iotas --toggle-extended-preferences`
2. Enable _Support Maths Equations_ in the preferences

The TeX support is new and the heavy lifting is done by [KaTeX](https://katex.org/). If you find this useful please leave [a note in an issue](https://gitlab.gnome.org/World/iotas/-/issues) to let us know it's working as expected. This will also help to ensure TeX has a life ongoing in Iotas.

Notes: 
- Use dollar sign delimiters
- While `/tag` should work as expected labels do not

**How do I view the extended preferences?**

Their visibility can be toggled with `flatpak run org.gnome.World.Iotas --toggle-extended-preferences`.

Note: The extended preferences are an experiment and the preferences and features bound to them may be removed at any point. Feedback is welcome.

## Development

Iotas was conceived in response to the question: what can be used to write simple notes on mobile Linux with fast sync to a self-hosted FLOSS server?

It's fairly minimal by design, so changes to integrate complex functionality may not be merged. Probably best to drop a note to say hi (in an issue) if you're interested in working on something sizeable and don't want to risk wasted effort. The focus is on Nextcloud Notes sync, and a good experience for the current generation of Linux mobile devices is higher priority than a feature-rich desktop app.

Iotas follows the [GNOME Code of Conduct](https://wiki.gnome.org/Foundation/CodeOfConduct) for all communications and contributions.

### Authentication storage

Iotas uses the Secret Service to store authentication details for Nextcloud. This is typically handled smoothly by GNOME Keyring but that may not be the case on mobile or other environments. KWallet is [reported](https://gitlab.gnome.org/World/iotas/-/issues/106) to be working as of Iotas v0.2.7.

## Thanks

Much was learnt and borrowed from projects Secrets, Feeds, Lollypop, Apostrophe, Fractal, Text Editor, Paper and Nextcloud Notes for Android. Many thanks!

## Why "Iotas"?

An iota is a little bit and this app is designed for jotting down little things on little devices. Iota stems from the same Greek word as jot and is commonly used in negative statements eg. "not one iota of …", but we think the word has more to give. Maybe somebody will take note?
