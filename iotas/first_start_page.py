from gi.repository import Adw, Gtk


@Gtk.Template(resource_path="/org/gnome/World/Iotas/ui/first_start_page.ui")
class FirstStartPage(Adw.Bin):
    __gtype_name__ = "FirstStartPage"

    def __init__(self):
        super().__init__()
