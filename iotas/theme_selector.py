from gi.repository import Gio, Gtk

from typing import Optional

import iotas.config_manager


@Gtk.Template(resource_path="/org/gnome/World/Iotas/ui/theme_selector.ui")
class ThemeSelector(Gtk.Box):
    __gtype_name__ = "ThemeSelector"

    _follow = Gtk.Template.Child()
    _light = Gtk.Template.Child()
    _dark = Gtk.Template.Child()

    def __init__(self):
        super().__init__()
        self.__populate()
        iotas.config_manager.settings.connect(
            f"changed::{iotas.config_manager.STYLE}", self.__populate
        )

    def __populate(self, _settings: Optional[Gio.Settings] = None, _key: str = "") -> None:
        style = iotas.config_manager.get_style()
        self._follow.set_active(style == "follow")
        self._light.set_active(style == "light")
        self._dark.set_active(style == "dark")
