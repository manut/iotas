from gi.repository import Adw, GObject, Gtk


@Gtk.Template(resource_path="/org/gnome/World/Iotas/ui/export_dialog_format_row.ui")
class ExportDialogFormatRow(Adw.ActionRow):
    __gtype_name__ = "ExportDialogFormatRow"

    def __init__(self):
        super().__init__()
        self.__out_format = ""
        self.__file_extension = ""

    @GObject.Property(type=str)
    def out_format(self) -> str:
        return self.__out_format

    @out_format.setter
    def out_format(self, value: str) -> None:
        self.__out_format = value
        self.__update_title()

    @GObject.Property(type=str)
    def file_extension(self) -> str:
        return self.__file_extension

    @file_extension.setter
    def file_extension(self, value: str) -> None:
        self.__file_extension = value
        self.__update_title()

    def __update_title(self) -> None:
        if self.__file_extension != "":
            new_title = self.__file_extension
        else:
            new_title = self.__out_format
        self.set_property("title", new_title.upper())
