import re


def sanitise_path(path: str) -> str:
    """Sanitise path for filesystem storage.

    :param str path: The path to work on
    :return: The sanitised path
    :rtype: str
    """
    # This is close to a direct translation of what Nextcloud Notes is using.

    # Remove characters which are illegal on Windows (includes illegal characters on Unix/Linux)
    # prevents also directory traversal by eliminiating slashes
    for token in ["*", "|", "/", "\\", ":", '"', "<", ">", "?"]:
        path = path.replace(token, "")

    # Prevent file being hidden
    path = re.sub(r"^[\.\s]+", "", path)

    return path.strip()
