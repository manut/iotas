from gi.repository import GObject, GLib, Gtk

import datetime
import logging
import os
import re
import shutil
from typing import Optional
import unicodedata

import pypandoc

import iotas.config_manager
from iotas import const
from iotas.html_generator import HtmlGenerator
from iotas.note import Note


class Exporter(GObject.Object):

    __gsignals__ = {
        # Out format, out path
        "finished": (GObject.SignalFlags.RUN_FIRST, None, (str, str)),
        # Out format and reason
        "failed": (GObject.SignalFlags.RUN_FIRST, None, (str, str)),
    }

    def __init__(self, webkit, html_generator: HtmlGenerator):
        super().__init__()
        self.__render_view = webkit
        self.__render_view.connect("loaded", self.__on_web_view_loaded)
        self.__html_generator = html_generator
        self.__active = False
        self.__in_error = False

    def export(
        self, note: Note, out_format: str, file_extension: str, user_location: Optional[str] = None
    ) -> None:
        """Export note.

        :param Note note: Note to render
        :param str out_format: Export format
        :param str file_extension: File extension
        :param str user_location: User chosen export location
        """
        self.__note = note
        self.__out_format = out_format
        if user_location:
            self.__location = user_location
        else:
            # Running with limited permissions in container, export to exports dir inside container
            # with automatic filename
            export_dir = os.path.join(GLib.get_user_data_dir(), "iotas", "exports")
            filename = self.build_default_filename(
                note, out_format, file_extension, add_timestamp=True
            )
            self.__location = os.path.join(export_dir, filename)
            if not os.path.exists(export_dir):
                try:
                    os.mkdir(export_dir)
                except OSError as e:
                    logging.warning(
                        f"Failed to export {out_format} to {self.__location}: %s", e.message
                    )
                    self.emit("failed", self.__out_format, e.message)
                    return

        if out_format == "pdf":
            self.__export_pdf()
        elif out_format == "md":
            self.__export_md()
        elif out_format == "html":
            self.__export_html()
        else:
            logging.info(f"Asking pandoc to export to {out_format}")
            self.__export_pandoc(out_format)

    def build_default_filename(
        self, note: Note, out_format: str, file_extension: str, add_timestamp: bool = False
    ) -> str:
        """Build an export filename for the note.

        :param Note note: Note to render
        :param str out_format: Export format
        :param str file_extension: File extension
        :param bool add_timestamp: Whether to prefix timestamp
        :return: Filename
        :rtype: str
        """
        filename = self.__sanitise_title_for_filename(note.title)
        if add_timestamp:
            ts = datetime.datetime.now()
            filename = ts.strftime("%Y-%m-%dT%H:%M:%S") + " " + filename
        if out_format != "html":
            filename += "." + file_extension
        return filename

    @GObject.Property(type=bool, default=False)
    def active(self) -> bool:
        return self.__active

    @active.setter
    def active(self, value: bool) -> None:
        self.__active = value

    def __on_web_view_loaded(self, _obj: GObject.Object) -> None:
        if self.__active and self.__out_format == "pdf":
            self.__export_pdf_post_render()

    def __export_pandoc(self, out_format: str) -> None:
        self.__active = True
        try:
            pypandoc.convert_text(
                self.__note.content, out_format, format="gfm", outputfile=self.__location
            )
        except (RuntimeError, OSError) as e:
            logging.warning(f"Failed to export {out_format} to {self.__location}: %s", e)
            self.emit("failed", self.__out_format, str(e))
        else:
            logging.info(f"Exported {self.__out_format} to {self.__location}")
            self.emit("finished", self.__out_format, os.path.dirname(self.__location))
        self.__active = False

    def __export_pdf(self) -> None:
        self.__active = True

        # Part of the effort to delay all WebKit initialisation
        if "WebKit" not in globals():
            import gi

            gi.require_version("WebKit", "6.0")
            global WebKit
            from gi.repository import WebKit

        self.__render_view.render_retaining_scroll(self.__note, "pdf")

    def __export_pdf_post_render(self) -> None:
        settings = Gtk.PrintSettings()
        settings.set(Gtk.PRINT_SETTINGS_PRINTER, "Print to File")
        settings.set(Gtk.PRINT_SETTINGS_OUTPUT_FILE_FORMAT, "pdf")
        settings.set(Gtk.PRINT_SETTINGS_OUTPUT_URI, f"file://{self.__location}")

        page_setup = Gtk.PageSetup()
        page_setup.set_left_margin(54, Gtk.Unit.POINTS)
        page_setup.set_right_margin(54, Gtk.Unit.POINTS)

        operation = WebKit.PrintOperation.new(self.__render_view)
        operation.set_print_settings(settings)
        operation.set_page_setup(page_setup)

        def finished(print_operation):
            logging.info(f"Exported {self.__out_format} to {self.__location}")
            if not iotas.config_manager.get_markdown_keep_webkit_process():
                logging.info("Terminating WebKit process as holding disabled in preference")
                self.__render_view.terminate_web_process()
            if not self.__in_error:
                self.emit("finished", self.__out_format, os.path.dirname(self.__location))
            self.__active = False

        def failed(print_operation, error: GLib.Error):
            logging.warning(
                f"Failed to export {self.__out_format} to {self.__location}: %s", error.message
            )
            if not iotas.config_manager.get_markdown_keep_webkit_process():
                logging.info("Terminating WebKit process as holding disabled in preference")
                self.__render_view.terminate_web_process()
            self.__in_error = True
            self.emit("failed", self.__out_format, error.message)

        operation.connect("finished", finished)
        operation.connect("failed", failed)
        operation.print_()

    def __export_md(self) -> None:
        self.__active = True

        try:
            with open(self.__location, "w") as f:
                f.write(self.__note.content)
        except OSError as e:
            self.emit("failed", self.__out_format, e.message)
            logging.warning("Failed to export MD: %s", e.message)
            self.__active = False
            return

        self.__active = False
        logging.info(f"Exported {self.__out_format} to {self.__location}")
        self.emit("finished", self.__out_format, os.path.dirname(self.__location))

    def __export_html(self) -> None:
        self.__active = True

        (content, _) = self.__html_generator.generate(
            self.__note, searching=False, export_format="html"
        )

        try:
            os.mkdir(self.__location)
        except OSError as e:
            self.emit("failed", self.__out_format, e)
            logging.warning("Failed to export HTML: %s", e)
            self.__active = False
            return

        index_filename = os.path.join(self.__location, "index.html")

        try:
            with open(index_filename, "w") as f:
                f.write(content)
        except OSError as e:
            self.emit("failed", self.__out_format, e.message)
            logging.warning("Failed to export HTML: %s", e.message)
            self.__active = False
            return

        css_dir = os.path.join(self.__location, "css")

        try:
            os.mkdir(css_dir)
        except OSError as e:
            self.emit("failed", self.__out_format, e.message)
            logging.warning("Failed to export HTML: %s", e.message)
            self.__active = False
            return

        dest_file = os.path.join(css_dir, os.path.basename(HtmlGenerator.RESOURCE_CSS_PATH))

        try:
            shutil.copyfile(f"{const.PKGDATADIR}/{HtmlGenerator.RESOURCE_CSS_PATH}", dest_file)
        except OSError as e:
            self.emit("failed", self.__out_format, e.message)
            logging.warning("Failed to export HTML: %s", e.message)
            self.__active = False
            return

        if iotas.config_manager.get_markdown_tex_support():
            dest_file = os.path.join(
                css_dir, os.path.basename(HtmlGenerator.RESOURCE_KATEX_CSS_PATH)
            )

            try:
                shutil.copyfile(
                    f"{const.PKGDATADIR}/{HtmlGenerator.RESOURCE_KATEX_CSS_PATH}", dest_file
                )
            except OSError as e:
                self.emit("failed", self.__out_format, e.message)
                logging.warning("Failed to export HTML: %s", e.message)
                self.__active = False
                return

            js_dir = os.path.join(self.__location, "js")

            try:
                os.mkdir(js_dir)
            except OSError as e:
                self.emit("failed", self.__out_format, e.message)
                logging.warning("Failed to export HTML: %s", e.message)
                self.__active = False
                return

            dest_file = os.path.join(
                js_dir, os.path.basename(HtmlGenerator.RESOURCE_KATEX_CSS_PATH)
            )

            try:
                shutil.copyfile(
                    f"{const.PKGDATADIR}/{HtmlGenerator.RESOURCE_KATEX_JS_PATH}", dest_file
                )
            except OSError as e:
                self.emit("failed", self.__out_format, e.message)
                logging.warning("Failed to export HTML: %s", e.message)
                self.__active = False
                return

        self.__active = False
        logging.info(f"Exported {self.__out_format} to {self.__location}")
        self.emit("finished", self.__out_format, self.__location)

    def __sanitise_title_for_filename(self, title: str) -> str:
        """For synced notes the server has already done the sanitising for us. This is for
        local-only instances.
        """
        value = unicodedata.normalize("NFKC", str(title))
        return re.sub(r"[^\w\s\.-]", "", value).strip()
