from gi.repository import GObject

from iotas.category_header_bar import CategoryHeaderBar
from iotas.editor import Editor
from iotas.editor_rename_header_bar import EditorRenameHeaderBar
from iotas.editor_search_entry import EditorSearchEntry
from iotas.editor_search_header_bar import EditorSearchHeaderBar
from iotas.editor_text_view import EditorTextView
from iotas.export_dialog_format_row import ExportDialogFormatRow
from iotas.first_start_page import FirstStartPage
from iotas.font_size_selector import FontSizeSelector
from iotas.index import Index
from iotas.index_menu_button import IndexMenuButton
from iotas.index_note_list import IndexNoteList
from iotas.index_search_header_bar import IndexSearchHeaderBar
from iotas.render_search_header_bar import RenderSearchHeaderBar
from iotas.revealer_notification import RevealerNotification
from iotas.selection_header_bar import SelectionHeaderBar
from iotas.sidebar import Sidebar
from iotas.sidebar_row import SidebarRow
from iotas.theme_selector import ThemeSelector
from iotas.timed_revealer_notification import TimedRevealerNotification


def load_widgets() -> None:
    """Ensure that the types have been registered with the type system.

    Allows them to be used in template UI files.
    """
    GObject.type_ensure(CategoryHeaderBar)
    GObject.type_ensure(Editor)
    GObject.type_ensure(EditorRenameHeaderBar)
    GObject.type_ensure(EditorSearchEntry)
    GObject.type_ensure(EditorSearchHeaderBar)
    GObject.type_ensure(EditorTextView)
    GObject.type_ensure(ExportDialogFormatRow)
    GObject.type_ensure(FirstStartPage)
    GObject.type_ensure(FontSizeSelector)
    GObject.type_ensure(Index)
    GObject.type_ensure(IndexMenuButton)
    GObject.type_ensure(IndexNoteList)
    GObject.type_ensure(IndexSearchHeaderBar)
    GObject.type_ensure(RenderSearchHeaderBar)
    GObject.type_ensure(RevealerNotification)
    GObject.type_ensure(SelectionHeaderBar)
    GObject.type_ensure(Sidebar)
    GObject.type_ensure(SidebarRow)
    GObject.type_ensure(ThemeSelector)
    GObject.type_ensure(TimedRevealerNotification)
