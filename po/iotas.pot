# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-05-28 20:10+1000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"

#. Translators: Iotas is the app name, do not translate
#: data/org.gnome.World.Iotas.desktop.in.in:4
msgid "Iotas"
msgstr ""

#. Translators: App description/comment in .desktop file
#: data/org.gnome.World.Iotas.desktop.in.in:6
msgid "Simple note taking with Nextcloud Notes"
msgstr ""

#. Translators: Search terms to find this application. Do NOT translate or localize the semicolons! The list MUST also end with a semicolon!
#: data/org.gnome.World.Iotas.desktop.in.in:14
msgid ""
"notes;nextcloud;minimal;distraction;editor;focused;text;write;markdown;"
"document;"
msgstr ""

#. Translators: The application's summary / tagline
#: data/org.gnome.World.Iotas.metainfo.xml.in.in:11
msgid "Simple note taking"
msgstr ""

#. Translators: Part of metainfo description. "Iotas" is the application name; do not translate.
#: data/org.gnome.World.Iotas.metainfo.xml.in.in:52
msgid ""
"Iotas aims to provide distraction-free note taking via its mobile-first "
"design."
msgstr ""

#. Translators: Part of metainfo description
#: data/org.gnome.World.Iotas.metainfo.xml.in.in:54
msgid "Featuring"
msgstr ""

#. Translators: Part of metainfo description
#: data/org.gnome.World.Iotas.metainfo.xml.in.in:57
msgid "Optional speedy sync with Nextcloud Notes"
msgstr ""

#. Translators: Part of metainfo description
#: data/org.gnome.World.Iotas.metainfo.xml.in.in:59
msgid "Offline note editing, syncing when back online"
msgstr ""

#. Translators: Part of metainfo description
#: data/org.gnome.World.Iotas.metainfo.xml.in.in:61
msgid "Category editing and filtering"
msgstr ""

#. Translators: Part of metainfo description
#. Translators: Section title
#: data/org.gnome.World.Iotas.metainfo.xml.in.in:63
#: data/ui/index_note_list.ui:17
msgid "Favorites"
msgstr ""

#. Translators: Part of metainfo description
#: data/org.gnome.World.Iotas.metainfo.xml.in.in:65
msgid "Spell checking"
msgstr ""

#. Translators: Part of metainfo description
#: data/org.gnome.World.Iotas.metainfo.xml.in.in:67
msgid "Search within the collection or individual notes"
msgstr ""

#. Translators: Part of metainfo description
#: data/org.gnome.World.Iotas.metainfo.xml.in.in:69
msgid "Focus mode and an option to hide the editor header bar"
msgstr ""

#. Translators: Part of metainfo description
#: data/org.gnome.World.Iotas.metainfo.xml.in.in:71
msgid "In preview: export to PDF, ODT and HTML"
msgstr ""

#. Translators: Part of metainfo description
#: data/org.gnome.World.Iotas.metainfo.xml.in.in:73
msgid "A convergent design, seeing Iotas as at home on desktop as mobile"
msgstr ""

#. Translators: Part of metainfo description
#: data/org.gnome.World.Iotas.metainfo.xml.in.in:75
msgid "Search from GNOME Shell"
msgstr ""

#. Translators: Part of metainfo description
#: data/org.gnome.World.Iotas.metainfo.xml.in.in:77
msgid "Note backup and restoration (from CLI, for using without sync)"
msgstr ""

#. Translators: Part of metainfo description
#: data/org.gnome.World.Iotas.metainfo.xml.in.in:79
msgid "The ability to change font size and toggle monospace style"
msgstr ""

#. Translators: Part of metainfo description
#: data/org.gnome.World.Iotas.metainfo.xml.in.in:82
msgid "Writing in markdown is supported but optional, providing"
msgstr ""

#. Translators: Part of metainfo description
#: data/org.gnome.World.Iotas.metainfo.xml.in.in:85
msgid "Syntax highlighting with themes"
msgstr ""

#. Translators: Part of metainfo description
#: data/org.gnome.World.Iotas.metainfo.xml.in.in:87
msgid "A formatted view"
msgstr ""

#. Translators: Part of metainfo description
#: data/org.gnome.World.Iotas.metainfo.xml.in.in:89
msgid "The ability to check off task lists from the formatted view"
msgstr ""

#. Translators: Part of metainfo description
#: data/org.gnome.World.Iotas.metainfo.xml.in.in:92
msgid "Slightly more technical details, for those into that type of thing"
msgstr ""

#. Translators: Part of metainfo description
#: data/org.gnome.World.Iotas.metainfo.xml.in.in:95
msgid ""
"Nextcloud Notes sync is via the REST API, not WebDAV, which makes it snappy"
msgstr ""

#. Translators: Part of metainfo description
#: data/org.gnome.World.Iotas.metainfo.xml.in.in:97
msgid "There's basic sync conflict detection"
msgstr ""

#. Translators: Part of metainfo description
#: data/org.gnome.World.Iotas.metainfo.xml.in.in:99
msgid "Notes are constantly saved"
msgstr ""

#. Translators: Part of metainfo description
#: data/org.gnome.World.Iotas.metainfo.xml.in.in:101
msgid "Large note collections are partially loaded to quicken startup"
msgstr ""

#. Translators: Part of metainfo description
#: data/org.gnome.World.Iotas.metainfo.xml.in.in:103
msgid ""
"Notes are stored in SQLite, providing for fast search (FTS) without "
"reinventing the wheel. Plain files can be retrieved by making a backup (CLI)."
msgstr ""

#. Translators: Part of metainfo description. "Iotas" is the application name, do not translate. Left to your discretion whether it makes sense to translate "iota" or not.
#: data/org.gnome.World.Iotas.metainfo.xml.in.in:106
msgid ""
"Why \"Iotas\"? An iota is a little bit and this app is designed for jotting "
"down little things on little devices. Iota stems from the same Greek word as "
"jot and is commonly used in negative statements eg. \"not one iota of …\", "
"but we think the word has more to give. Maybe somebody will take note?"
msgstr ""

#. Translators: A screenshot description.
#. Translators: Title
#: data/org.gnome.World.Iotas.metainfo.xml.in.in:112
#: data/ui/keyboard_shortcuts_window.ui:12 data/ui/preferences_dialog.ui:14
msgid "Index"
msgstr ""

#. Translators: A screenshot description.
#: data/org.gnome.World.Iotas.metainfo.xml.in.in:117
msgid "Editor with markdown"
msgstr ""

#. Translators: A screenshot description.
#: data/org.gnome.World.Iotas.metainfo.xml.in.in:122
msgid "Rendered markdown"
msgstr ""

#. Translators: A screenshot description.
#: data/org.gnome.World.Iotas.metainfo.xml.in.in:127
msgid "Index in dark style"
msgstr ""

#. Translators: A screenshot description.
#: data/org.gnome.World.Iotas.metainfo.xml.in.in:132
msgid "Mobile"
msgstr ""

#. Add your name to the translator credits list
#: data/ui/about_dialog.ui.in:13
msgid "translator-credits"
msgstr ""

#. Translators: Button
#: data/ui/category_header_bar.ui:11 data/ui/editor_rename_header_bar.ui:13
msgid "Revert changes"
msgstr ""

#. Translators: Button tooltip
#: data/ui/category_header_bar.ui:38 data/ui/editor_rename_header_bar.ui:36
msgid "Apply changes"
msgstr ""

#. Translators: Button
#: data/ui/category_header_bar.ui:40 data/ui/editor_rename_header_bar.ui:38
msgid "Apply"
msgstr ""

#. Translators: Button
#: data/ui/category_header_bar.ui:54
msgid "Clear and apply"
msgstr ""

#. Translators: Placeholder text
#. Translators: Menu item
#: data/ui/editor_search_entry.ui:14 data/ui/editor.ui:25
msgid "Find"
msgstr ""

#. Translators: Button
#: data/ui/editor_search_header_bar.ui:12 data/ui/index_search_header_bar.ui:11
#: data/ui/render_search_header_bar.ui:11 data/ui/selection_header_bar.ui:16
msgid "Back"
msgstr ""

#. Translators: Button tooltip
#. Translators: Description, keyboard shortcut
#. Translators: Button tooltip
#: data/ui/editor_search_header_bar.ui:39
#: data/ui/keyboard_shortcuts_window.ui:169
#: data/ui/render_search_header_bar.ui:32
msgid "Previous match"
msgstr ""

#. Translators: Button tooltip
#. Translators: Description, keyboard shortcut
#. Translators: Button tooltip
#: data/ui/editor_search_header_bar.ui:47
#: data/ui/keyboard_shortcuts_window.ui:162
#: data/ui/render_search_header_bar.ui:40
msgid "Next match"
msgstr ""

#. Translators: Placeholder text
#. Translators: Button
#. Translators: Description, keyboard shortcut
#: data/ui/editor_search_header_bar.ui:69
#: data/ui/editor_search_header_bar.ui:78
#: data/ui/keyboard_shortcuts_window.ui:155
msgid "Replace"
msgstr ""

#. Translators: Menu item
#. Translators: Description, keyboard shortcut
#: data/ui/editor.ui:18 data/ui/keyboard_shortcuts_window.ui:182
msgid "Focus mode"
msgstr ""

#. Translators: Menu item
#. Translators: Description, keyboard shortcut
#: data/ui/editor.ui:32 data/ui/keyboard_shortcuts_window.ui:86
msgid "Edit title"
msgstr ""

#. Translators: Menu item
#. Translators: Description, keyboard shortcut
#: data/ui/editor.ui:37 data/ui/keyboard_shortcuts_window.ui:107
msgid "Change category"
msgstr ""

#. Translators: Menu item
#: data/ui/editor.ui:42
msgid "Delete"
msgstr ""

#. Translators: Menu item
#. Translators: Description, keyboard shortcut
#. Translators: Button
#: data/ui/editor.ui:49 data/ui/keyboard_shortcuts_window.ui:93
#: iotas/export_dialog.py:119
msgid "Export"
msgstr ""

#. Translators: Button
#: data/ui/editor.ui:65
msgid "Back to notes"
msgstr ""

#. Translators: Button
#. Translators: Description, keyboard shortcut
#: data/ui/editor.ui:109 data/ui/keyboard_shortcuts_window.ui:114
msgid "Toggle markdown render"
msgstr ""

#. Translators: Button
#: data/ui/editor.ui:117
msgid "Edit note"
msgstr ""

#. Translators: Description
#: data/ui/editor.ui:143
msgid "Read-only note"
msgstr ""

#. Translators: Description, help
#: data/ui/editor.ui:211
msgid "Render engine loading"
msgstr ""

#. Translators: Title
#: data/ui/empty_state.ui:11
msgid "Let's get started"
msgstr ""

#. Translators: Description, help
#: data/ui/empty_state.ui:43
msgid "Add new feeds via URL"
msgstr ""

#. Translators: Description, help
#: data/ui/empty_state.ui:54
msgid "Import an OPML file"
msgstr ""

#. Translators: Button
#: data/ui/export_dialog.ui:21 data/ui/nextcloud_login_dialog.ui:21
msgid "Cancel"
msgstr ""

#. Translators: Title
#: data/ui/export_dialog.ui:29
msgid "Export As..."
msgstr ""

#. Translators: Title
#: data/ui/export_dialog.ui:49
msgid "Exporting..."
msgstr ""

#. Translators: Button
#: data/ui/export_dialog.ui:68 data/ui/export_dialog.ui:95
#: iotas/ui_utils.py:112
msgid "Close"
msgstr ""

#. Translators: Button
#: data/ui/export_dialog.ui:75
msgid "Show"
msgstr ""

#. Translators: Title. Iotas is the application name and shouldn't be translated.
#: data/ui/first_start_page.ui:10
msgid "Welcome to Iotas"
msgstr ""

#. Translators: Description, introduction help
#: data/ui/first_start_page.ui:29
msgid "Add a note"
msgstr ""

#. Translators: Description, introduction help
#. Translators: Menu item
#: data/ui/first_start_page.ui:49 data/ui/index_menu_button.ui:13
msgid "Sync with Nextcloud Notes"
msgstr ""

#. Translators: Button tooltip
#. Translators: Button
#: data/ui/font_size_selector.ui:15 data/ui/font_size_selector.ui:19
msgid "Zoom out"
msgstr ""

#. Translators: Button tooltip
#. Translators: Button
#: data/ui/font_size_selector.ui:35 data/ui/font_size_selector.ui:39
msgid "Zoom in"
msgstr ""

#. Translators: Menu item
#: data/ui/index_menu_button.ui:19
msgid "Refresh"
msgstr ""

#. Translators: Menu item
#: data/ui/index_menu_button.ui:27
msgid "Preferences"
msgstr ""

#. Translators: Menu item
#: data/ui/index_menu_button.ui:32
msgid "Keyboard Shortcuts"
msgstr ""

#. Translators: Menu item, Iotas is the application name and shouldn't be translated
#: data/ui/index_menu_button.ui:37
msgid "About Iotas"
msgstr ""

#. Translators: Button
#: data/ui/index_menu_button.ui:44
msgid "Menu"
msgstr ""

#. Translators: Section title
#: data/ui/index_note_list.ui:58
msgid "Today"
msgstr ""

#. Translators: Section title
#: data/ui/index_note_list.ui:90
msgid "Yesterday"
msgstr ""

#. Translators: Section title
#: data/ui/index_note_list.ui:122
msgid "This Week"
msgstr ""

#. Translators: Section title
#: data/ui/index_note_list.ui:154
msgid "This Month"
msgstr ""

#. Translators: Section title
#: data/ui/index_note_list.ui:186
msgid "Last Month"
msgstr ""

#. Translators: Button
#: data/ui/index_note_list.ui:217
msgid "Show earlier months"
msgstr ""

#. Translators: Section title
#: data/ui/index_note_list.ui:235
msgid "Before Last Month"
msgstr ""

#. Translators: Button
#: data/ui/index_note_list.ui:267
msgid "Show more"
msgstr ""

#: data/ui/index.ui:20
msgid "Categories"
msgstr ""

#. Translators: Button
#: data/ui/index.ui:41
msgid "Open categories"
msgstr ""

#. Translators: Button
#: data/ui/index.ui:49
msgid "New note"
msgstr ""

#. Translators: Button
#. Translators: Description, keyboard shortcut
#: data/ui/index.ui:68 data/ui/keyboard_shortcuts_window.ui:24
#: data/ui/keyboard_shortcuts_window.ui:148
msgid "Search"
msgstr ""

#. Translators: Button
#: data/ui/index.ui:76
msgid "Select notes"
msgstr ""

#. Translators: Description
#: data/ui/index.ui:94
msgid "Server connection offline"
msgstr ""

#. Translators: Description
#: data/ui/index.ui:100
msgid ""
"Due to behind-the-scenes changes (a new app id) Iotas needs to "
"reauthenticate with your Nextcloud server"
msgstr ""

#. Translators: Button
#: data/ui/index.ui:102 data/ui/index.ui:111
msgid "Authenticate"
msgstr ""

#. Translators: Description
#: data/ui/index.ui:109
msgid ""
"The authentication token for sync with Nextcloud Notes could not be retrieved"
msgstr ""

#. Translators: Description, help
#: data/ui/index.ui:134
msgid "Note list empty"
msgstr ""

#. Translators: Description, help
#: data/ui/index.ui:141
msgid "Enter search term"
msgstr ""

#. Translators: Description, help
#: data/ui/index.ui:148
msgid "No search results"
msgstr ""

#. Translators: Description, keyboard shortcut
#: data/ui/keyboard_shortcuts_window.ui:17
msgid "Create new note"
msgstr ""

#. Translators: Description, keyboard shortcut
#: data/ui/keyboard_shortcuts_window.ui:31
msgid "Show sidebar"
msgstr ""

#. Translators: Description, keyboard shortcut
#: data/ui/keyboard_shortcuts_window.ui:38
msgid "Delete note"
msgstr ""

#. Translators: Description, keyboard shortcut
#: data/ui/keyboard_shortcuts_window.ui:45
msgid "Move up list"
msgstr ""

#. Translators: Description, keyboard shortcut
#: data/ui/keyboard_shortcuts_window.ui:52
msgid "Move down list"
msgstr ""

#. Translators: Description, keyboard shortcut
#: data/ui/keyboard_shortcuts_window.ui:59
msgid "Start selection"
msgstr ""

#. Translators: Description, keyboard shortcut
#: data/ui/keyboard_shortcuts_window.ui:66
msgid "Reset filter"
msgstr ""

#. Translators: Description, keyboard shortcut
#: data/ui/keyboard_shortcuts_window.ui:73
msgid "Open first search result"
msgstr ""

#. Translators: Title
#: data/ui/keyboard_shortcuts_window.ui:81 data/ui/preferences_dialog.ui:36
msgid "Editor"
msgstr ""

#. Translators: Description, keyboard shortcut
#: data/ui/keyboard_shortcuts_window.ui:100
msgid "Create new note including selection"
msgstr ""

#. Translators: Description, keyboard shortcut
#: data/ui/keyboard_shortcuts_window.ui:121
msgid "Undo typing"
msgstr ""

#. Translators: Description, keyboard shortcut
#: data/ui/keyboard_shortcuts_window.ui:128
msgid "Redo typing"
msgstr ""

#. Translators: Description, keyboard shortcut
#: data/ui/keyboard_shortcuts_window.ui:135
msgid "Return to Index"
msgstr ""

#. Translators: Title
#: data/ui/keyboard_shortcuts_window.ui:143
msgid "Editor Search"
msgstr ""

#. Translators: Title
#: data/ui/keyboard_shortcuts_window.ui:177
msgid "Editor Appearance"
msgstr ""

#. Translators: Description, keyboard shortcut
#: data/ui/keyboard_shortcuts_window.ui:189
msgid "Increase line length"
msgstr ""

#. Translators: Description, keyboard shortcut
#: data/ui/keyboard_shortcuts_window.ui:196
msgid "Decrease line length"
msgstr ""

#. Translators: Description, keyboard shortcut
#: data/ui/keyboard_shortcuts_window.ui:203
msgid "Increase font size"
msgstr ""

#. Translators: Description, keyboard shortcut
#: data/ui/keyboard_shortcuts_window.ui:210
msgid "Decrease font size"
msgstr ""

#. Translators: Title
#: data/ui/keyboard_shortcuts_window.ui:218
msgid "General"
msgstr ""

#. Translators: Description, keyboard shortcut
#: data/ui/keyboard_shortcuts_window.ui:223
msgid "Toggle fullscreen"
msgstr ""

#. Translators: Description, keyboard shortcut
#: data/ui/keyboard_shortcuts_window.ui:230
msgid "Show preferences"
msgstr ""

#. Translators: Description, keyboard shortcut
#: data/ui/keyboard_shortcuts_window.ui:237
msgid "Show shortcuts"
msgstr ""

#. Translators: Description, keyboard shortcut
#: data/ui/keyboard_shortcuts_window.ui:244
msgid "Go back"
msgstr ""

#. Translators: Description, keyboard shortcut
#: data/ui/keyboard_shortcuts_window.ui:251
msgid "Quit"
msgstr ""

#. Translators: Button
#: data/ui/nextcloud_login_dialog.ui:30
msgid "Continue"
msgstr ""

#. Translators: Description
#: data/ui/nextcloud_login_dialog.ui:67
msgid ""
"Press Continue to provide your Nextcloud server address and login via a web "
"browser"
msgstr ""

#. Translators: Description
#: data/ui/nextcloud_login_dialog.ui:95
msgid ""
"The Secret Service could not be accessed for storing authentication details. "
"Ensure you have a provider such as gnome-keyring. A default keyring needs to "
"be setup, and that keyring unlocked. Most desktop environments will provide "
"this for you. Restart the app to try again."
msgstr ""

#. Translators: Description
#: data/ui/nextcloud_login_dialog.ui:116
msgid "Nextcloud Server URL"
msgstr ""

#. Translators: Description
#: data/ui/nextcloud_login_dialog.ui:157
msgid ""
"You appear to be using a self-signed SSL certificate resulting in the server "
"identity not being verified. If this is expected please follow the "
"instructions in the FAQ to provide a CA chain file."
msgstr ""

#. Translators: Button
#: data/ui/nextcloud_login_dialog.ui:163
msgid "Open the FAQ"
msgstr ""

#. Translators: Description
#: data/ui/nextcloud_login_dialog.ui:188
msgid "Connecting"
msgstr ""

#. Translators: Description
#: data/ui/nextcloud_login_dialog.ui:195
msgid "Waiting for completion of login in browser"
msgstr ""

#. Translators: Description
#: data/ui/nextcloud_login_dialog.ui:202
msgid "Performing initial transfer"
msgstr ""

#. Translators: Description
#: data/ui/nextcloud_login_dialog.ui:209
msgid "Updating notes"
msgstr ""

#. Translators: Description
#: data/ui/nextcloud_login_dialog.ui:242
msgid "Connection established with Nextcloud Notes"
msgstr ""

#. Translators: Title
#: data/ui/preferences_dialog.ui:10
msgid "Interface"
msgstr ""

#. Translators: Title
#: data/ui/preferences_dialog.ui:18
msgid "Automatically Expand Sidebar"
msgstr ""

#. Translators: Description, preference
#: data/ui/preferences_dialog.ui:20
msgid "On desktop, when there is space."
msgstr ""

#. Translators: Description, preference
#: data/ui/preferences_dialog.ui:28
msgid "Category Label Style"
msgstr ""

#. Translators: Title
#: data/ui/preferences_dialog.ui:40
msgid "Use Monospace Font"
msgstr ""

#. Translators: Title
#: data/ui/preferences_dialog.ui:47
msgid "Check Spelling"
msgstr ""

#. Translators: Title
#: data/ui/preferences_dialog.ui:54
msgid "Hide Headerbar"
msgstr ""

#. Translators: Title
#: data/ui/preferences_dialog.ui:61
msgid "Hide Headerbar When Fullscreen"
msgstr ""

#. Translators: Title
#: data/ui/preferences_dialog.ui:68
msgid "Limit Line Length"
msgstr ""

#. Translators: Description, preference
#: data/ui/preferences_dialog.ui:71
msgid ""
"Primarily for desktop. Use Ctrl + ↑ and Ctrl + ↓ on keyboard to fine tune."
msgstr ""

#. Translators: Title
#: data/ui/preferences_dialog.ui:79
msgid "Markdown"
msgstr ""

#. Translators: Title
#: data/ui/preferences_dialog.ui:83
msgid "Highlight Syntax"
msgstr ""

#. Translators: Description, preference
#: data/ui/preferences_dialog.ui:85
msgid "Disable for slightly improved performance."
msgstr ""

#. Translators: Description, preference
#: data/ui/preferences_dialog.ui:92
msgid "Syntax Theme"
msgstr ""

#. Translators: Title
#: data/ui/preferences_dialog.ui:98
msgid "Enable Formatted View"
msgstr ""

#. Translators: Description, preference
#: data/ui/preferences_dialog.ui:100
msgid "Disable to reduce startup time."
msgstr ""

#. Translators: Title
#: data/ui/preferences_dialog.ui:107
msgid "Open In Formatted View"
msgstr ""

#. Translators: Description, preference
#: data/ui/preferences_dialog.ui:109
msgid "Enabling opens all notes as rendered markdown."
msgstr ""

#. Translators: Title
#: data/ui/preferences_dialog.ui:116
msgid "Support Math Equations"
msgstr ""

#. Translators: Description, preference
#: data/ui/preferences_dialog.ui:118
msgid "Slightly decreases render performance."
msgstr ""

#. Translators: Title
#: data/ui/preferences_dialog.ui:125
msgid "Render Using Monospace Font"
msgstr ""

#. Translators: Title
#: data/ui/preferences_dialog.ui:132
msgid "Proportional To Monospace Font Size Ratio"
msgstr ""

#: data/ui/preferences_dialog.ui:133
msgid "In render view. Use 1 for no adjustment."
msgstr ""

#. Translators: Title
#: data/ui/preferences_dialog.ui:149
msgid "Hold Engine In Memory"
msgstr ""

#. Translators: Description, preference
#: data/ui/preferences_dialog.ui:151
msgid "Faster subsequent conversions for higher memory usage."
msgstr ""

#. Translators: Title
#: data/ui/preferences_dialog.ui:164
msgid "Data"
msgstr ""

#. Translators: Title
#: data/ui/preferences_dialog.ui:170
msgid "Reset Database"
msgstr ""

#. Translators: Description, preference
#: data/ui/preferences_dialog.ui:172
msgid "Delete all notes from the local database. The app will quit."
msgstr ""

#. Translators: Title
#: data/ui/preferences_dialog.ui:190
msgid "Disconnect Nextcloud"
msgstr ""

#. Translators: Description, preference
#: data/ui/preferences_dialog.ui:192
msgid ""
"Signs out from Nextcloud Notes. All notes will be removed and the app will "
"quit."
msgstr ""

#. Translators: Title
#: data/ui/preferences_dialog.ui:216
msgid "Debug"
msgstr ""

#. Translators: Title
#: data/ui/preferences_dialog.ui:222
msgid "Clear Sync Timestamp"
msgstr ""

#. Translators: Description, preference
#: data/ui/preferences_dialog.ui:224
msgid "Forces a pull of all notes from the sync server."
msgstr ""

#. Translators: Button
#: data/ui/selection_header_bar.ui:32
msgid "Delete selected"
msgstr ""

#. Translators: Button
#: data/ui/selection_header_bar.ui:43
msgid "Toggle favorite for selected"
msgstr ""

#. Translators: Button
#: data/ui/selection_header_bar.ui:51
msgid "Change category for selected"
msgstr ""

#. Translators: Button
#: data/ui/sidebar.ui:13
msgid "Close categories"
msgstr ""

#. Translators: Description, tooltip
#. Translators: Description, accessibility
#: data/ui/theme_selector.ui:26 data/ui/theme_selector.ui:29
msgid "Follow system style"
msgstr ""

#. Translators: Description, tooltip
#. Translators: Description, accessibility
#: data/ui/theme_selector.ui:45 data/ui/theme_selector.ui:48
msgid "Light style"
msgstr ""

#. Translators: Description, tooltip
#. Translators: Description, accessibility
#: data/ui/theme_selector.ui:64 data/ui/theme_selector.ui:67
msgid "Dark style"
msgstr ""

#. Translators: Description, CLI option
#: iotas/application.py:236
msgid "Create a note"
msgstr ""

#. Translators: Description, CLI option
#: iotas/application.py:245
msgid "Create a backup"
msgstr ""

#. Translators: Description, CLI option
#: iotas/application.py:254
msgid "Restore a backup"
msgstr ""

#. Translators: Description, CLI option
#: iotas/application.py:263
msgid "Display backup path"
msgstr ""

#. Translators: Description, CLI option
#: iotas/application.py:272
msgid "Display path for custom server SSL CA chain file"
msgstr ""

#. Translators: Description, CLI option
#: iotas/application.py:281
msgid "Toggle display of extended preferences in UI"
msgstr ""

#. Translators: Description, CLI option
#: iotas/application.py:290
msgid "Quit any running instance"
msgstr ""

#. Translators: Description, CLI option
#: iotas/application.py:299
msgid "Enable debug logging and functions"
msgstr ""

#. Translators: Description, CLI option
#: iotas/application.py:308
msgid "Open note by id"
msgstr ""

#. Translators: Description, CLI option
#: iotas/application.py:317
msgid "Search in notes"
msgstr ""

#. Translators: Description, prefixes note title on backup restoration clash
#: iotas/backup_manager.py:185
msgid "RESTORATION REMOTE ID CLASH"
msgstr ""

#. Duplicate note
#. Translators: Description, prefixes note title on backup restoration clash
#: iotas/backup_manager.py:203
msgid "RESTORATION TITLE CLASH"
msgstr ""

#. Translators: Title
#: iotas/category.py:19
msgid "All notes"
msgstr ""

#. Translators: Title
#: iotas/category.py:21
msgid "Uncategorised"
msgstr ""

#. Translators: Description, notification
#: iotas/editor.py:680
msgid "Line length limit already disabled"
msgstr ""

#. Translators: Description, notification
#: iotas/editor.py:690
msgid "Line length limit disabled"
msgstr ""

#. Translators: Description, notification, {0} is a number
#: iotas/editor.py:764
#, python-brace-format
msgid "Line length now {0}px"
msgstr ""

#. Translators: Description, notification, {0} is a number
#: iotas/editor.py:770
#, python-brace-format
msgid "Font size now {0}pt"
msgstr ""

#. Translators: Description, {0} the current position in {1} a number of search results
#: iotas/editor_search_entry.py:66
#, python-brace-format
msgid "{0} of {1}"
msgstr ""

#. Translators: Description, {} is a format eg. PDF
#: iotas/export_dialog.py:91
msgid "Exported to {}"
msgstr ""

#. Translators: Description, {} is a format eg. PDF
#: iotas/export_dialog.py:104
msgid "Failed to export to {}"
msgstr ""

#. Translators: Description, notification
#: iotas/index.py:91 iotas/index.py:813
msgid "Loading"
msgstr ""

#. Translators: Description, notification, {} is a positive number
#: iotas/index.py:187
msgid "{} notes deleted"
msgstr ""

#. Translators: Description, notification
#: iotas/index.py:190
msgid "Note deleted"
msgstr ""

#. Translators: Button
#: iotas/index.py:195
msgid "Undo"
msgstr ""

#. Translators: Description, notification
#: iotas/index.py:235
msgid "Sync conflict with note being edited"
msgstr ""

#. Translators: Description, notification. "Secret Service" and "gnome-keyring"
#. should likely not be translated.
#: iotas/index.py:245
msgid ""
"Failure accessing Secret Service. Ensure you have a provider like gnome-"
"keyring which has a default keyring setup that is unlocked."
msgstr ""

#. Translators: Button
#: iotas/index.py:251
msgid "OK"
msgstr ""

#. Translators: Description, notification
#: iotas/index.py:500
msgid "Syncing"
msgstr ""

#. Translators: Description, notification, {} is a number
#: iotas/index.py:510
msgid "{} change"
msgid_plural "{} changes"
msgstr[0] ""
msgstr[1] ""

#. Translators: Description, notification
#: iotas/index.py:527
msgid "Sync failure. Is the Nextcloud Notes app installed on the server?"
msgstr ""

#. Translators: Button
#: iotas/nextcloud_login_dialog.py:124
msgid "Finish"
msgstr ""

#. Translators: Description, notification
#: iotas/nextcloud_login_dialog.py:218
msgid "Failed to start login with possible certificate issue"
msgstr ""

#. Translators: Description, notification
#: iotas/nextcloud_login_dialog.py:221
msgid "Failed to start login. Wrong address?"
msgstr ""

#. Translators: Description, a visual style (for category labels in index)
#: iotas/preferences_dialog.py:79
msgid "Monochrome"
msgstr ""

#. Translators: Description, a visual style (for category labels in index)
#: iotas/preferences_dialog.py:81
msgid "Muted"
msgstr ""

#. Translators: Description, a visual style (for category labels in index)
#: iotas/preferences_dialog.py:83
msgid "Blue"
msgstr ""

#. Translators: Description, a visual style (for category labels in index)
#: iotas/preferences_dialog.py:85
msgid "Orange"
msgstr ""

#. Translators: Description, a visual style (for category labels in index)
#: iotas/preferences_dialog.py:87
msgid "Red"
msgstr ""

#. Translators: Description, a visual style (for category labels in index)
#: iotas/preferences_dialog.py:89
msgid "None"
msgstr ""

#. Translators: Description, notification, {0} is a number
#: iotas/preferences_dialog.py:222
#, python-brace-format
msgid "Reducing in {0} presses"
msgstr ""

#. Translators: Description, notification, {0} is a number
#: iotas/preferences_dialog.py:225
#, python-brace-format
msgid "Extending in {0} presses"
msgstr ""

#. Translators: Description, notification
#: iotas/preferences_dialog.py:230
msgid "Extended hidden"
msgstr ""

#. Translators: Description, notification
#: iotas/preferences_dialog.py:233
msgid "Extended shown"
msgstr ""

#. Translators: Description, alert
#: iotas/selection_header_bar.py:86 iotas/selection_header_bar.py:182
msgid "Unable to change category on read-only note"
msgstr ""

#. Translators: Description, {} is a number
#: iotas/selection_header_bar.py:165
msgid "{} Selected"
msgstr ""

#. Translators: Description, used as a prefix to the previous title for notes updated both
#. locally and remotely. " - " is placed between this prefix and the title.
#: iotas/sync_manager.py:456
msgid "SYNC CONFLICT"
msgstr ""

#. Translators: Title
#: iotas/ui_utils.py:110
msgid "Error"
msgstr ""
